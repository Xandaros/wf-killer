#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/keyboard.h>
#include <linux/module.h>
#include <linux/notifier.h>
#include <linux/pid.h>
#include <linux/rcupdate.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Xandaros");
MODULE_DESCRIPTION("Warframe killer");
MODULE_VERSION("0.01");

int sysrq = 0;

void kill_wf(void) {
	struct task_struct *task_list;

	for_each_process(task_list) {
		if (strcmp(task_list->comm, "Warframe.x64.ex") == 0) {
			printk(KERN_INFO "Killing warframe process with PID %i", task_list->pid);

			kill_pid(find_vpid(task_list->pid), SIGKILL, 1);
			return;
		}
	}
}

int keyb_callback(struct notifier_block *block, unsigned long action, void *data) {
	struct keyboard_notifier_param *param = data;
	if (action != KBD_KEYCODE) return NOTIFY_DONE;

	if (param->value == 17 && param->shift == 4) {
		// ctrl+w
		if (param->down == 0){ 
			//printk(KERN_INFO "Keyboard action: %li", action);
			//printk(KERN_INFO "value: %i", param->value);
			//printk(KERN_INFO "shift: %i", param->shift);
			//printk(KERN_INFO "down: %i", param->down);
			//printk(KERN_INFO "sysrq: %i", sysrq);
			//printk(KERN_INFO "Done\n\n");

			if (sysrq) {
				rcu_read_lock();
				kill_wf();
				rcu_read_unlock();
			}
		}
		return NOTIFY_STOP;
	} else if (param->value == 99 && param->shift == 4) {
		sysrq = param->down;
		return NOTIFY_STOP;
	}
	return NOTIFY_DONE;
}

struct notifier_block notifier = {
	.notifier_call = keyb_callback,
	.next = NULL,
	.priority = 0
};

static int __init mod_init(void) {
	register_keyboard_notifier(&notifier);
	printk(KERN_INFO "WF-Killer loaded\n");
	return 0;
}

static void __exit mod_exit(void) {
	unregister_keyboard_notifier(&notifier);
	printk(KERN_INFO "WF-Killer unloaded\n");
}

module_init(mod_init);
module_exit(mod_exit);
